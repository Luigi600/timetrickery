package net.studio.lui.timetrickery.util.string;

public final class StringPlaceholder {
    private final String placeholder;
    private final String fill;

    public String getPlaceholder() {
        return this.placeholder;
    }

    public String getFill() {
        return this.fill;
    }
    
    public StringPlaceholder(final String placeholder, final String fill) {
        this.placeholder = placeholder;
        this.fill = fill;
    }
}
