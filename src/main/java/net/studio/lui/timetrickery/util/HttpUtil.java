package net.studio.lui.timetrickery.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.stream.Collectors;

public final class HttpUtil {
    private static final int API_CONNECTION_TIMEOUT = 1000 * 15; // in ms

    private HttpUtil() {
    }

    public static String getStringFromURL(final String url) throws IOException {
        URL api = new URL(url);
        URLConnection urlConnection = api.openConnection();
        urlConnection.setConnectTimeout(API_CONNECTION_TIMEOUT);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        urlConnection.getInputStream()
                )
        );
        final String result = in.lines().collect(Collectors.joining("\n"));
        in.close();
        return result;
    }
}
