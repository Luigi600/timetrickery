package net.studio.lui.timetrickery.util.string;

import org.bukkit.ChatColor;

public final class StringUtil {
    private StringUtil() {
    }

    public static String color(final String toColor) {
        return color(toColor, '&');
    }

    public static String color(final String toColor, final char alt) {
        return ChatColor.translateAlternateColorCodes(alt, toColor);
    }
}
