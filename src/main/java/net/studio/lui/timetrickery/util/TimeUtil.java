package net.studio.lui.timetrickery.util;

import net.studio.lui.timetrickery.constants.Time;

public final class TimeUtil {
    private TimeUtil() { }

    public static boolean isDay(long tick) {
        long tickDay = tick % Time.DAY;

        return (tickDay >= Time.BEGINNING && tickDay <= Time.NIGHT);
    }

    public static double getDays(long tick) {
        return tick / (double)Time.DAY;
    }
}
