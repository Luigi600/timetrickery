package net.studio.lui.timetrickery.models;

import net.studio.lui.timetrickery.constants.Time;
import org.bukkit.World;

public final class TimedWorld {
    /**
     * Specifies the world whose time is to be calculated.
     */
    private final World world;

    /**
     * Specifies the configuration for the time to be calculated.
     */
    private final WorldConfig config;

    /**
     * Gets the world which time is being calculated.
     * @return Returns the world.
     */
    public World getWorld() {
        return world;
    }

    public WorldConfig getConfig() {
        return this.config;
    }

    /**
     *
     * @param world Specifies the world whose time is to be calculated.
     * @param config Specifies the configuration of the world for the time to be calculated.
     */
    public TimedWorld(World world, long startTick, WorldConfig config) {
        this.world = world;
        this.config = config;
    }

    /**
     * Updates the start tick depending on the lifetime of the world from which the time is to be calculated. By
     * default, this is 0. However, when sleeping or set (`time set XYZ`) the value must be updated so that the
     * time continues to be calculated from the set time.
     * @param newTick Specifies the tick from which the time is to be calculated.
     */
    public void updateStartTick(long newTick) {
        this.config.setStartTick(newTick);
    }

    /**
     * Calculates the actual hours in Vanilla format based on the factors (Day & Night) and the life span of the world.
     * @return
     */
    public long getHoursFromLifetime() {
        final long time = world.getGameTime() - this.config.getStartTick();
        final long oneFactoredFullDay = (long)this.convertDaysToFactoredTicks(1);
        long hours = time % oneFactoredFullDay;

        return this.convertFactoredHoursToVanillaHours(hours);
    }

    /**
     * Converts the specified hours from factorised to vanilla based on factors (day & night).
     * @param hours Specifies the factorised hours to be converted to vanilla format.
     * @return
     */
    public long convertFactoredHoursToVanillaHours(long hours) {
        long factoredHours = (long)(hours / this.config.getFactorDay());
        long newTime = 0;

        // check if "day" (sun-time) is over
        if(factoredHours >= Time.NIGHT) {
            factoredHours -= Time.NIGHT; // remove the sun-time from hours
            factoredHours *= this.config.getFactorDay(); // denormalize

            newTime += Time.NIGHT; // add the whole sun-time to result
            newTime += factoredHours / this.config.getFactorNight(); // convert rest of hours with the night factor
        }
        else {
            newTime += factoredHours; // just add the sun-time
        }

        // for the case that the result is bigger than one day => modulo
        return newTime % Time.DAY;
    }

    /**
     * Converts the specified hours from vanilla format to factored hours based on the factors (day & night).
     * @param hours Specifies the hours in vanilla format to be converted.
     * @return
     */
    public long convertVanillaHoursToFactoredHours(long hours) {
        long newTime = 0;

        // check if "day" (sun-time) is over
        if(hours >= Time.NIGHT) {
            newTime += Time.NIGHT * this.config.getFactorDay(); // add the whole sun-time to result
            hours -= Time.NIGHT;
            newTime += hours * this.config.getFactorNight(); // convert rest of hours with the night factor
        }
        else {
            newTime += hours * this.config.getFactorDay(); // just add the sun-time
        }

        // for the case that the result is bigger than one day => modulo
        return (long) (newTime % this.convertDaysToFactoredTicks(1));
    }

    // should be the same as `convertVanillaHoursToFactoredHours(Time.DAY)`
    public float convertDaysToFactoredTicks(long tick) {
        return (
                (tick * Time.DAY) * Time.RATIO_DAY_NIGHT * this.config.getFactorDay() +
                (tick * Time.DAY) * (1 - Time.RATIO_DAY_NIGHT) * this.config.getFactorNight()
        );
    }
}
