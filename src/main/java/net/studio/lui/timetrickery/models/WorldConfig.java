package net.studio.lui.timetrickery.models;

import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public final class WorldConfig implements ConfigurationSerializable, Cloneable {
    private static final float EPS_ZERO = 0.000001f;

    /**
     * Specifies the tick from which the time is to be calculated. This value changes when sleeping or setting the time
     * via the command.
     * See {@link TimedWorld#updateStartTick(long)}.
     */
    private long startTick;

    /**
     * Specifies the factor by which the day is to be stretched.
     */
    private final float factorDay;

    /**
     * Specifies the factor by which the night is to be stretched.
     */
    private final float factorNight;

    private boolean useDefaultFactorDay = false;
    private boolean useDefaultFactorNight = false;

    public long getStartTick() {
        return startTick;
    }

    public void setStartTick(long startTick) {
        this.startTick = startTick;
    }

    public float getFactorDay() {
        return factorDay;
    }

    public float getFactorNight() {
        return factorNight;
    }

    /**
     *
     * @param startTick
     * @param factorDay Specifies the factor by which the day is to be stretched.
     * @param factorNight Specifies the factor by which the night is to be stretched.
     */
    public WorldConfig(long startTick, float factorDay, float factorNight) {
        this.startTick = startTick;
        this.factorDay = factorDay;
        this.factorNight = factorNight;
    }

    public WorldConfig(int startTick, float factorDay, float factorNight) {
        this((long)startTick, factorDay, factorNight);
    }

    public WorldConfig(Map<String, Object> map)
    {
        // parse string necessary coz map contains LONG OR INT and FLOAT OR DOUBLE (not consistent)
        this(
                Long.parseLong(String.valueOf(map.get("startTick"))),
                Float.parseFloat(String.valueOf(map.getOrDefault("day", 0.0f))),
                Float.parseFloat(String.valueOf(map.getOrDefault("night", 0.0f)))
        );

        this.useDefaultFactorDay = (boolean) map.getOrDefault("defaultFactorDay", false);
        this.useDefaultFactorNight = (boolean) map.getOrDefault("defaultFactorNight", false);
    }

    @NotNull
    @Override
    public Map<String, Object> serialize() {
        return this.serialize(false);
    }

    public Map<String, Object> serialize(boolean exportHelperVars) {
        HashMap<String, Object> result = new HashMap<>();

        result.put("startTick", this.startTick);
        if(Math.abs(this.factorDay) > EPS_ZERO && !this.useDefaultFactorDay) {
            result.put("day", this.factorDay);

            if(exportHelperVars)
                result.put("defaultFactorDay", false);
        }

        if(Math.abs(this.factorNight) > EPS_ZERO && !this.useDefaultFactorNight) {
            result.put("night", this.factorNight);

            if(exportHelperVars)
                result.put("defaultFactorNight", false);
        }

        return result;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
