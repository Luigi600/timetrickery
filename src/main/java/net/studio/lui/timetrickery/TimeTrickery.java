package net.studio.lui.timetrickery;

import com.github.zafarkhaja.semver.Version;
import net.studio.lui.timetrickery.config.ConfigManager;
import net.studio.lui.timetrickery.config.ConfigNodes;
import net.studio.lui.timetrickery.managers.WorldTimeManager;
import net.studio.lui.timetrickery.models.WorldConfig;
import net.studio.lui.timetrickery.update.GitLabUpdateChecker;
import org.bukkit.Bukkit;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class TimeTrickery extends JavaPlugin {
    private final static String URL_PLUGIN_DOWNLOAD = "https://gitlab.com/Luigi600/timetrickery/-/releases";

    /**
     * Specifies the number of ticks at which the calculation for the actual day/night is to be calculated and set.
     *
     * The shorter the factor value of the worlds, the smaller this value should be. The larger the factor, the larger this value can be.
     */
    private final static int INTERVAL_CHECK = 20;

    private final static List<String> OFFICIAL_SUPPORTED_VERSIONS = new ArrayList<>() {
        {
            // "patch" number always "0", coz I will ignore the patch number
            add("1.21.0");
            add("1.20.0");
        }
    };

    @Override
    public void onEnable() {
        // source: https://www.spigotmc.org/threads/custom-configurationserializable.567870/#post-4459137
        ConfigurationSerialization.registerClass(WorldConfig.class);

        final PluginManager pluginManager = Bukkit.getPluginManager();

        final ConfigManager configManager = new ConfigManager(this, "1.0.0", "config.yml");
        configManager.defaults();

        final Optional<Version> version = Version.tryParse(getServer().getBukkitVersion());
        final String apiVersion = version.isEmpty()
                ? null
                : new Version.Builder()
                    .setMajorVersion(version.get().majorVersion())
                    .setMinorVersion(version.get().minorVersion())
                    .build().toString();

        if(!OFFICIAL_SUPPORTED_VERSIONS.contains(apiVersion)) {
            getLogger().warning(
                    String.format(
                            "You have a version that does not officially support Minecraft version %s. Deactivate the plugin if problems occur or check for updates.",
                            apiVersion
                    )
            );
        }

        final WorldTimeManager manager = new WorldTimeManager(configManager);
        manager.updateAllTimes();

        pluginManager.registerEvents(manager, this);

        // no event per tick, so runnable
        // source:
        //   - https://bukkit.org/threads/listener-based-on-time.31924/#post-585515
        //   - https://www.spigotmc.org/threads/is-there-a-gametick-or-servertick-event.273711/#post-2671075
        //   - https://bukkit.org/threads/time-changing-check.331258/#post-2954185
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                manager.updateAllTimes();
            }
        }, 0, INTERVAL_CHECK);

        this.checkOnUpdates(configManager);
    }

    public String getVersion() {
        final String packageName = this.getServer().getClass().getPackage().getName();
        return packageName.substring(packageName.lastIndexOf('.') + 1);
    }

    private void checkOnUpdates(ConfigManager configManager) {
        GitLabUpdateChecker updateChecker = new GitLabUpdateChecker(
                this,
                configManager.getBoolean(ConfigNodes.UPDATES_ENABLED)
        );

        switch (updateChecker.check()) {
            case OUT_OF_DATE:
                getLogger().warning(
                    String.format(
                        "A new version (%s) is available on %s.",
                        updateChecker.getNewVersion().toString(),
                        URL_PLUGIN_DOWNLOAD
                    )
                );
                break;
            case UP_TO_DATE:
                getLogger().info(
                    "Uses the latest version."
                );
                break;
            case EMPTY_LIST:
                getLogger().warning(
                    String.format(
                            "The list of versions of the plugin are empty. Update check failed! Please check manually for updates (%s).",
                            URL_PLUGIN_DOWNLOAD
                    )
                );
                break;
            case INVALID_PLUGIN_VERSION_SCHEMA:
                getLogger().warning(
                    String.format(
                            "The version of the plugin does not match the scheme of a version. Update check failed! Please check manually for updates (%s).",
                            URL_PLUGIN_DOWNLOAD
                    )
                );
                break;
            default:
                break;
        }
    }
}
