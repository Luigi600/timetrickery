package net.studio.lui.timetrickery.update;

public enum EUpdateState {
    DISABLED,
    OUT_OF_DATE,
    UP_TO_DATE,
    EMPTY_LIST,
    INVALID_PLUGIN_VERSION_SCHEMA,
}
