package net.studio.lui.timetrickery.update;

import com.github.zafarkhaja.semver.Version;
import net.studio.lui.timetrickery.util.HttpUtil;
import org.bukkit.plugin.java.JavaPlugin;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class GitLabUpdateChecker {
    private final JavaPlugin plugin;
    private final boolean enabled;
    private final List<Version> versions = new ArrayList<>();

    public GitLabUpdateChecker(JavaPlugin plugin, boolean enabled) {
        this.plugin = plugin;
        this.enabled = enabled;
    }

    public EUpdateState check() {
        if (!this.enabled)
            return EUpdateState.DISABLED;

        Optional<Version> currentVersion = Version.tryParse(this.plugin.getDescription().getVersion());
        if (currentVersion.isEmpty())
            return EUpdateState.INVALID_PLUGIN_VERSION_SCHEMA;

        this.getVersionsFromRepository();

        if (versions.size() == 0)
            return EUpdateState.EMPTY_LIST;

        if (currentVersion.get().isLowerThan(versions.get(0)))
            return EUpdateState.OUT_OF_DATE;

        return EUpdateState.UP_TO_DATE;
    }

    public Version getNewVersion() {
        return this.versions.get(0);
    }

    private void getVersionsFromRepository() {
        this.versions.clear();

        try {
            String jsonString = HttpUtil.getStringFromURL(Constants.API_RELEASES_URL);
            JSONArray jsonArray = new JSONArray(jsonString);
            for (int i = 0; i < jsonArray.length(); ++i) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                String version = jsonObject.getString(Constants.GITLAB_RELEASE_JSON_OBJECT_TAG_NAME);
                // check if release candidate
                if (version.matches(Constants.REGEX_RELEASE_CANDIDATE))
                    continue;

                // remove "version" prefix
                if(version.startsWith("v"))
                    version = version.substring(1);

                Optional<Version> parsedVersion = Version.tryParse(version, false);
                parsedVersion.ifPresent(versions::add);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        Collections.sort(versions);
    }
}
