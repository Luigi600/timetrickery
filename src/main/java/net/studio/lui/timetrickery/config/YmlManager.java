package net.studio.lui.timetrickery.config;

import com.google.common.base.Charsets;
import net.studio.lui.timetrickery.util.string.StringUtil;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.FileUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class YmlManager {
    protected final JavaPlugin plugin;
    protected final String fileName;
    protected FileConfiguration conf;

    protected YmlManager(final JavaPlugin plugin, final String fileName) {
        this.plugin = plugin;
        this.fileName = fileName;
    }

    protected abstract boolean equalsVersion(FileConfiguration conf);

    public boolean defaults() {
        final File configFile = this.getConfigFile();
        if (!configFile.exists()) {
            this.saveDefault();
            return false;
        }
        this.reload(); // set this.conf
        if (this.equalsVersion(this.conf))
            return false;

        this.backup();
        return true;
    }

    public void reload() {
        final File configFile = this.getConfigFile();
        if (!configFile.exists()) {
            this.saveDefault();
            return;
        }
        this.conf = YamlConfiguration.loadConfiguration(configFile);
    }

    public void backup() {
        final File configFile = this.getConfigFile();
        FileUtil.copy(configFile, new File(configFile.getParentFile(), this.fileName + ".backup"));
        this.saveDefault();
    }

    public void saveDefault() {
        final File configFile = this.getConfigFile();
        final InputStream stream = this.plugin.getResource(this.fileName);
        this.conf = YamlConfiguration.loadConfiguration(new InputStreamReader(stream, Charsets.UTF_8));
        try {
            this.conf.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getPrefix() {
        return this.getString("messages.prefix");
    }

    public String getLanguageValue(final String node) {
        return this.getPrefix() + StringUtil.color(this.getString(node));
    }

    public String getLanguageList(final String node) {
        final List<String> lists = this.conf.getStringList(node).
                stream().
                map(StringUtil::color).
                collect(Collectors.toList());
        return String.join("\n", lists);
    }

    public String getString(final String node) {
        final String retrieve = this.conf.getString(node);
        return StringUtil.color(retrieve);
    }

    public boolean getBoolean(final String node) {
        return this.conf.getBoolean(node);
    }

    @Nullable
    public <T> T getObject(final String node, Class<T> object) {
        return this.conf.getObject(node, object);
    }

    public @NotNull
    List<String> getStringList(final String node) {
        return this.conf.getStringList(node);
    }

    public Set<String> getListOfKeys(final String node) {
        ConfigurationSection configurationSection = this.conf.getConfigurationSection(node);
        if (configurationSection == null)
            return new HashSet<>(0);

        return configurationSection.getKeys(false);
    }

    public File getConfigFile() {
        final File folder = this.plugin.getDataFolder();
        if (!folder.exists())
            folder.mkdirs();

        return new File(folder, this.fileName);
    }

    public void setValueAndSave(String path, Object object) {
        this.conf.set(path, object);

        try {
            this.conf.save(this.getConfigFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public <K, V> void setValueAndSave(String path, Map<K, V> object) {
        this.conf.createSection(path, object);

        try {
            this.conf.save(this.getConfigFile());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
