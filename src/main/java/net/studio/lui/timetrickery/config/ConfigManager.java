package net.studio.lui.timetrickery.config;

import com.github.zafarkhaja.semver.Version;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;

public class ConfigManager extends YmlManager {
    private final Version supportedConfigVersion;

    public ConfigManager(final JavaPlugin plugin, final String supportedConfigVersion, final String fileName) {
        super(plugin, fileName);

        final Optional<Version> version = Version.tryParse(supportedConfigVersion);

        if(version.isEmpty()) {
            throw new RuntimeException("");
        }

        this.supportedConfigVersion = version.get();
    }

    @Override
    protected boolean equalsVersion(FileConfiguration conf) {
        final Optional<Version> configVersion = Version.tryParse((String)this.conf.get("config-version"));

        if(configVersion.isEmpty()) return false;

        return configVersion.get().isEquivalentTo(this.supportedConfigVersion);
    }
}
