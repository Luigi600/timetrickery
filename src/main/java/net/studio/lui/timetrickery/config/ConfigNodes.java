package net.studio.lui.timetrickery.config;

public final class ConfigNodes {
    private ConfigNodes() {
    }

    //options
    public static final String DEFAULT_FACTORS = "options.factors.*";
    public static final String UPDATES_ENABLED = "options.update-checker.enabled";

    public static String getFactorPath(String worldName) {
        return String.format("%s%s", DEFAULT_FACTORS.substring(0, DEFAULT_FACTORS.length() - 1), worldName);
    }
}
