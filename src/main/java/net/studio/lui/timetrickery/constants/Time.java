package net.studio.lui.timetrickery.constants;

/**
 * @source https://minecraft.fandom.com/wiki/Daylight_cycle
 */
public final class Time {
    private Time() {}

    public static final long BEGINNING = 0;
    public static final long NOON = 6000;
    public static final long SUNSET = 12000;
    public static final long NIGHT = 13000;
    public static final long MIDNIGHT = 18000;
    public static final long SUNRISE = 23000;

    public static final long DAY = 24000;

    public static final float RATIO_DAY_NIGHT = NIGHT / (float)DAY;
}
