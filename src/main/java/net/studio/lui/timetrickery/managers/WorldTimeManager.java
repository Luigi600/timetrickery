package net.studio.lui.timetrickery.managers;

import net.studio.lui.timetrickery.config.ConfigManager;
import net.studio.lui.timetrickery.config.ConfigNodes;
import net.studio.lui.timetrickery.models.WorldConfig;
import net.studio.lui.timetrickery.models.TimedWorld;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.TimeSkipEvent;
import org.bukkit.event.world.WorldLoadEvent;
import org.bukkit.event.world.WorldUnloadEvent;

import java.util.*;

public class WorldTimeManager implements Listener {
    private final ConfigManager configManager;
    private final WorldConfig defaultConfig;
    private final ArrayList<TimedWorld> worlds = new ArrayList<>();
    private boolean triggerTimeChanged = false;

    public WorldTimeManager(ConfigManager configManager) {
        this.configManager = configManager;
        this.defaultConfig = this.configManager.getObject(ConfigNodes.DEFAULT_FACTORS, WorldConfig.class);

        for(World world : Bukkit.getServer().getWorlds()) {
            this.addWorld(world);
        }
    }

    // source: https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/event/world/TimeSkipEvent.html
    @EventHandler(priority = EventPriority.MONITOR)
    public void onTimeChanged(TimeSkipEvent event) {
        if(event.isCancelled() || this.triggerTimeChanged) return;
        Optional<TimedWorld> world = worlds.stream().filter(w -> w.getWorld().equals(event.getWorld())).findFirst();
        if(world.isPresent()) {
            long newTime = event.getWorld().getGameTime();
            long newHours = event.getWorld().getFullTime() + event.getSkipAmount();
            newTime -= world.get().convertVanillaHoursToFactoredHours(newHours);
            world.get().updateStartTick(newTime);
            this.configManager.setValueAndSave(ConfigNodes.getFactorPath(event.getWorld().getName()), world.get().getConfig());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onWorldAdded(WorldLoadEvent event) {
        this.addWorld(event.getWorld());
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onWorldRemoved(WorldUnloadEvent event) {
        if(event.isCancelled()) return;

        int index = 0;
        while(index < this.worlds.size()) {
            TimedWorld world = this.worlds.get(index);
            if(world.getWorld().equals(event.getWorld())) {
                this.worlds.remove(index);
            }
            else {
                ++index;
            }
        }
    }

    public void updateAllTimes() {
        // necessary to use `setFullTime` coz
        // > Note that setting the relative time below the current relative time will actually move the clock forward a day. ( https://hub.spigotmc.org/javadocs/bukkit/org/bukkit/World.html#setTime(long) )

        this.triggerTimeChanged = true;

        for(TimedWorld world : this.worlds) {
            long newTime = world.getHoursFromLifetime();

            if(world.getWorld().getFullTime() != newTime)
                world.getWorld().setFullTime(newTime);
        }

        this.triggerTimeChanged = false;
    }

    private void addWorld(World world) {
        String factorName = ConfigNodes.getFactorPath(world.getName());

        // default Map
        HashMap<String, Object> values = new HashMap<>(this.defaultConfig.serialize());
        values.put("defaultFactorDay", true);
        values.put("defaultFactorNight", true);

        // overwrite default map with custom values (if present)
        final WorldConfig config = this.configManager.getObject(factorName, WorldConfig.class);
        if(config != null)
            values.putAll(config.serialize(true));

        WorldConfig worldConfig = new WorldConfig(values);

        this.worlds.add(new TimedWorld(world, 0, worldConfig));
    }
}
