# Time T(r)ickery

Time T(r)ickery is a small helper to change the day-night cycle of a server. The plugin can be used to lengthen or shorten the days and nights - and it can be configured individually for the world!

<br />

## Support

| Spigot Version | Plugin Version |
|----------------|----------------|
| 1.20           | 1.0.0          |
| 1.21           | 1.0.1          |

<br />
<br />

## Installation

The installation of the plugin is very simple.

1. just go to the [GitLab releases](https://gitlab.com/Luigi600/timetrickery/-/releases), select the latest release (
   should be the top one) and click on "**TimeTrickery (JAR)**" under the "other" section
2. move the downloaded JAR file to your server's `plugins` folder
3. reload/restart the server

<br />
<br />

## Screenshots

<details>
  <summary>In ACTION!</summary>

![](https://gitlab.com/Luigi600/timetrickery/-/wikis/imgs/timetrickery.webp)
</details>


<br />
<br />

## Build

**Note:** This information is only of interest to a developer.

Java v15 is used because of the APIs (Bukkit, Spigot) and Maven is used as project management tool. To build the
application through automation (CI/CD), the Docker image `maven:3.8.4-openjdk-17` is used.

To build the JAR file:

```shell
mvn clean package
```

<br />
<br />

## Donation

If you like my work, you can donate for me. By doing so, you show that my work is valued. A donation is not mandatory,
but I would be very happy :)

| Service / Currency | Address                                    | Link / QR Code                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|--------------------|--------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Bitcoin            | 39S7NWad3FyGVN43etz4K45s9MBaYj8sbp         | ![Bitcoin Donation Address](.gitlab/doc/imgs/donate_bitcoin.png)                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| Ethereum           | 0xBf6722333D72Bd1C0B3C6B3ad970310D1Ea6E83B | ![Ethereum Donation Address](.gitlab/doc/imgs/donate_ethereum.png)                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| Litecoin           | MCi3b77HVTvJFAcDZ3wASsmJRJrmxGgzKj         | ![Litecoin Donation Address](.gitlab/doc/imgs/donate_litecoin.png)                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| PayPal             | payment [at] lui-studio.net                | ![PayPal Donation Address](.gitlab/doc/imgs/donate_paypal.png) <br /> <form action="https://www.paypal.com/donate" method="post" target="_top"><input type="hidden" name="hosted_button_id" value="MBKLD79X3SCT2" /><input type="image" src="https://www.paypalobjects.com/en_US/DE/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" /><img alt="" border="0" src="https://www.paypal.com/en_DE/i/scr/pixel.gif" width="1" height="1" /></form> |
